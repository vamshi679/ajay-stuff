// import express module

const exp = require("express");

const app = exp();

// body parser
app.use(exp.json());

// assign port no to server
const port = 3336;
app.listen(port, () => {
    console.log("server is running on", port);
});

// import mongodb client
const mc = require("mongodb").MongoClient;

// connecting to db
const dburl = "mongodb://b25db:ajay1996@cluster0-shard-00-00-bjzwt.mongodb.net:27017,cluster0-shard-00-01-bjzwt.mongodb.net:27017,cluster0-shard-00-02-bjzwt.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
var dbo;
mc.connect(dburl, { useUnifiedTopology: true, useNewUrlParser: true },
    (err, client) => {
        if (err) {
            console.log("error in connecting", err);
        }
        else {
            dbo = client.db("b25db");
            console.log("connected to db");
        }

    });
// connecting to fornt end
const path = require("path");
app.use(exp.static(path.join(__dirname,"./dist/app")))
// req handlers
// get req
app.get('/read', (req, res) => {
    dbo.collection("personcollection").find().toArray((err, arr) => {
        if (err) {
            console.log("error in find", err);
        }
        else if (arr.length == 0) {
            res.send({ message: " no persons" });
        }
        else {
            res.send({ message: arr });

        }
    });
});

// post req handler
app.post('/create', (req, res) => {
    console.log(req.body)
    dbo.collection("personcollection").findOne({ name: req.body.name }, (err, obj) => {
        if (err) {
            console.log("error in find", err);
        }
        else if (obj == null) {
            dbo.collection('personcollection').insertOne(req.body,(err,result)=>{
                if(err){
                    console.log('err in insert',err)
                }
                else{
                    res.send({message:'data inserted'})
                }
            })
        }
        else {
            res.send({ message: "person exist" });
        }
    });
});


// put req 

app.put('/update', (req, res) => {
   console.log(req.body);
   dbo.collection("personcollection").findOne({name:req.body.name},(err,obj)=>{
       if(err)
       {
           console.log("error in find",err);
       }
       else if(obj == null)
       {
     res.send({message:"person not existed"})
       }
       else
       {
        dbo.collection("personcollection").updateOne({ name: req.body.name },
            {
                $set: {
                    age: req.body.age,
                    gender: req.body.gender,
                    mobileno: req.body.mobileno
                }
            },
            (err, result) => {
    
                if (err) {
                    console.log("error in find", err);
                }
                else {
                    res.send({ message: "updated" });
                }
    
            })
       }
   });
    
})
// delete
app.delete('/delete/:name', (req, res) => {
    dbo.collection("personcollection").findOne({ name: req.params.name }, (err, obj) => {
        if (err) {
            console.log("Errer in find", err);
        }
        else if (obj == null) {
            res.send({ message: "person not exist" });
        }
        else {
            dbo.collection("personcollection").deleteOne({ name: req.params.name }, (err, result) => {
                if (err) {
                    console.log("Errer in delete", err);
                }
                else {
                    res.send({ message: "deleted" })
                }

            });
        }
    });
})