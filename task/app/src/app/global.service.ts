import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor( private hc : HttpClient) { }
  // post req
  save(obj): Observable<any>
  {
    console.log(obj)
   return this.hc.post('/create',obj);
  }
  // get data
  
  getdata():Observable<any>
  {
    return this.hc.get('/read');
  }
// update req
editdata(updobj):Observable<any>
{
return this.hc.put('/update',updobj);
}

// delete req
remove(delobj):Observable<any>
{
 return this.hc.delete(`/delete/${delobj.name}`);
}
}
