import { Component } from '@angular/core';
import { GlobalService } from './global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
constructor(private gs : GlobalService){}
submit(obj)
{
console.log(obj);
this.gs.save(obj).subscribe((response)=>{
  alert(response['message']);
  this.ngOnInit();
})
}
// get data
array : any;
ngOnInit()
{
  this.gs.getdata().subscribe((response)=>{
    this.array = response['message']
  })
}
// edit modal
editarr:any=[]
editmod(x)
{
 this.editarr=x;
}
// update data
update(updobj)
{
this.gs.editdata(updobj).subscribe((response)=>{
  alert(response['message']);
  this.ngOnInit();
})
}
// del req
delete(delobj)
{
  let a = confirm("are u sure to delete")
  if(a == true)
  {
    this.gs.remove(delobj).subscribe((response)=>{
    alert(response['message']);
    this.ngOnInit();
    })
  }
  else
  {
    this.ngOnInit();
  }
 
}


}
